<?php
/**
 * Contains the class for managing the Gravity Forms settings table.
 *
 * @package lh/gfrf
 */

/**
 * GFRF_Related_Fields_Table class. This class extends WP_List_Table to handle displaying the
 * list of related field connections.
 *
 * @since 1.0.0
 *
 * @see WP_List_Table
 */
class GFRF_Related_Fields_Table extends WP_List_Table {

	/**
	 * Stores the Gravity Form the connections belong to.
	 *
	 * @since 1.0.0
	 *
	 * @var array<mixed>
	 */
	public $form;

	/**
	 * Init table.
	 *
	 * @param array<mixed> $form Gravity Forms meta data.
	 */
	public function __construct( $form ) {

		$this->form = $form;

		$this->_column_headers = array(
			array(
				'cb'                => '',
				'target_field'      => __( 'Field', 'gravity-forms-related-fields' ),
				'source_form'       => __( 'Source form', 'gravity-forms-related-fields' ),
				'source_form_field' => __( 'Source form field', 'gravity-forms-related-fields' ),
			),
			array(),
			array(),
			'target_field',
		);

		parent::__construct();
	}

	/**
	 * Prepares the list of items for displaying.
	 */
	public function prepare_items(): void {
		$this->items = gfrf_get_related_fields( $this->form['id'] );
	}

	/**
	 * Displays the table.
	 */
	public function display(): void {
		$singular = rgar( $this->_args, 'singular' );
		?>
		<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" cellspacing="0">
			<thead>
			<tr>
			<?php $this->print_column_headers(); ?>
			</tr>
			</thead>

			<tfoot>
			<tr>
				<?php $this->print_column_headers( false ); ?>
			</tr>
			</tfoot>

			<tbody id="the-list"
			<?php
			if ( $singular ) {
				echo " class='list:$singular'"; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
			?>
			>

			<?php $this->display_rows_or_placeholder(); ?>

			</tbody>
		</table>

		<?php
	}

	/**
	 * Generates content for a single row of the table.
	 *
	 * @param object|array<mixed> $item The current item.
	 */
	public function single_row( $item ): void {
		static $row_class = '';
		$row_class        = ( '' === $row_class ? ' class="alternate"' : '' );

		echo '<tr id="confirmation-' . esc_attr( $item['id'] ) . '" ' . $row_class . '>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		$this->single_row_columns( $item );
		echo '</tr>';
	}

	/**
	 * Gets a list of columns.
	 *
	 * @return string[]
	 */
	public function get_columns() {
		return $this->_column_headers[0];
	}

	/**
	 * Outputs default column name.
	 *
	 * @param object|array<mixed> $item Related Field being displayed.
	 * @param string              $column Column key.
	 */
	public function column_default( $item, $column ): void {
		echo esc_html( rgar( $item, $column ) );
	}

	/**
	 * Display a toggle button to deactivate related field connections.
	 *
	 * @since 1.0.0
	 *
	 * @param object|array<mixed> $item Related Field being displayed.
	 */
	public function column_cb( $item ): void {

		$is_active = isset( $item['is_active'] ) ? $item['is_active'] : true;
		?>
		<img src="<?php echo esc_url( GFCommon::get_base_url() ); ?>/images/active<?php echo intval( $is_active ); ?>.png" style="cursor: pointer;margin:-5px 0 0 8px;" alt="<?php $is_active ? __( 'Active', 'gravity-forms-related-fields' ) : __( 'Inactive', 'gravity-forms-related-fields' ); ?>" title="<?php echo $is_active ? esc_attr__( 'Active', 'gravity-forms-related-fields' ) : esc_attr__( 'Inactive', 'gravity-forms-related-fields' ); ?>" onclick="gfrf_toggle_active(this, '<?php echo esc_js( $item['id'] ); ?>'); " />
		<?php
	}

	/**
	 * Used by Gravity Forms to field actions.
	 *
	 * @param object|array<mixed> $item Item being displayed.
	 */
	public function column_target_field( $item ): void {
		$edit_url = add_query_arg( array( 'rfid' => $item['id'] ) );
		$actions  = apply_filters(
			'gfrf_related_field_actions',
			array(
				'edit'   => '<a title="' . __( 'Edit this item', 'gravity-forms-related-fields' ) . '" href="' . esc_url( $edit_url ) . '">' . __( 'Edit', 'gravity-forms-related-fields' ) . '</a>',
				'delete' => '<a title="' . __( 'Delete this item', 'gravity-forms-related-fields' ) . '" class="submitdelete" onclick="javascript: if(confirm(\'' . __( 'WARNING: You are about to delete this related field connection.', 'gravity-forms-related-fields' ) . esc_js( __( "'Cancel' to stop, 'OK' to delete.", 'gravity-forms-related-fields' ) ) . '\')){ gfrf_delete_confirmation(\'' . esc_js( $item['id'] ) . '\'); }" style="cursor:pointer;">' . __( 'Delete', 'gravity-forms-related-fields' ) . '</a>',
			)
		);

		$field_label = '';
		foreach ( $this->form['fields'] as $field ) {
			if ( $field['id'] === $item['target_field_id'] ) {
				$field_label = $field['label'];
				break;
			}
		}

		?>

		<a href="<?php echo esc_url( $edit_url ); ?>"><strong><?php echo esc_html( $field_label ); ?></strong></a>
		<div class="row-actions">

			<?php
			if ( is_array( $actions ) && ! empty( $actions ) ) {
				$keys     = array_keys( $actions );
				$last_key = array_pop( $keys );
				foreach ( $actions as $key => $html ) {
					$divider = $key === $last_key ? '' : ' | ';
					?>
					<span class="<?php echo esc_attr( $key ); ?>">
						<?php echo $html . $divider; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</span>
					<?php
				}
			}
			?>

		</div>

		<?php
	}

	/**
	 * Used by Gravity Forms to print form title.
	 *
	 * @param object|array<mixed> $item Item being displayed.
	 */
	public function column_source_form( $item ): void {
		$form = GFFormsModel::get_form_meta( $item['source_form_id'] );

		if ( isset( $form['title'] ) ) {
			echo esc_html( $form['title'] );
		}
	}

	/**
	 * Used by Gravity Forms to print form fields.
	 *
	 * @param object|array<mixed> $item Item being displayed.
	 */
	public function column_source_form_field( $item ): void {
		$form = GFFormsModel::get_form_meta( $item['source_form_id'] );

		if ( ! is_array( $form['fields'] ) ) {
			return;
		}

		foreach ( $form['fields'] as $field ) {
			if ( $field['id'] === $item['source_form_field_id'] ) {
				echo empty( $field['adminLabel'] ) ? esc_html( $field['label'] ) : esc_html( $field['adminLabel'] );
				break;
			}
		}
	}
}
